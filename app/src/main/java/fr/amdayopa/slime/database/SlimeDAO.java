package fr.amdayopa.slime.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.amdayopa.slime.SlimeApplication;
import fr.amdayopa.slime.slimeBoi.Slime;
import fr.amdayopa.slime.utils.Pair;

public class SlimeDAO extends DAOBase {
    public static final String TABLE_NAME = "Slime";
    public static final String KEY = "id";
    public static final String NAME = "nom";
    public static final String LASTCO = "lastco";
    public static final String BARFAIM = "barfaim";
    public static final String BARJOIE = "barjoie";
    public static final String BAR3 = "bar3";

    public static final String TABLE_CREATE = "CREATE TABLE " + TABLE_NAME + " ("
            + KEY + " STRING PRIMARY KEY, "
            + NAME + " TEXT, "
            + LASTCO + " INTEGER, "
            + BARFAIM + " REAL, "
            + BARJOIE + " REAL, "
            + BAR3 + " REAL);";

    public static final String TABLE_DROP =  "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    public SlimeDAO(Context pContext) {
        super(pContext);
    }

    /**
     * @param s le slime à ajouter à la base
     */
    public void creer(Slime s) {
        ContentValues values = new ContentValues();
        values.put(SlimeDAO.KEY, s.getId().toString());
        values.put(SlimeDAO.NAME, s.getName());
        values.put(SlimeDAO.LASTCO, s.getLastConnection());
        values.put(SlimeDAO.BARFAIM, s.getBarFaim());
        values.put(SlimeDAO.BARJOIE, s.getBarJoie());
        mDb.insert(SlimeDAO.TABLE_NAME, null, values);
    }

    /**
     * @param id l'identifiant du slime à supprimer
     */
    public void supprimer(int id) {
        mDb.delete(TABLE_NAME, KEY + " = ?", new String[] {String.valueOf(id)});
    }

    /**
     * @param s le slime modifié
     */
    public void sauvegarder(Slime s) {
        ContentValues values = new ContentValues();
        values.put(SlimeDAO.NAME, s.getName());
        values.put(SlimeDAO.LASTCO, s.getLastConnection());
        values.put(SlimeDAO.BARFAIM, s.getBarFaim());
        values.put(SlimeDAO.BARJOIE, s.getBarJoie());
        mDb.update(TABLE_NAME, values, KEY  + " = ?", new String[] {s.getId().toString()});
    }

    /**
     * @param id l'identifiant du slime à récupérer
     * @return le Slime trouvé en bdd portant l'id
     */
    public Slime charger(UUID id) {
        Cursor cursor = mDb.rawQuery("select * from " + TABLE_NAME + " where id = ?", new String[]{id.toString()});
        Slime s = null;
        if(cursor.moveToFirst()) {
            UUID idslime = UUID.fromString(cursor.getString(0));
            String name = cursor.getString(1);
            long lastco = cursor.getLong(2);
            double barfaim = cursor.getDouble(3);
            double barjoie = cursor.getDouble(4);
            s = new Slime(idslime, name, lastco, barfaim, barjoie);
        }
        cursor.close();
        return s;
    }

    public static boolean loadMainSlime(Context c){
        SlimeDAO sdao = new SlimeDAO(c);
        sdao.open();
        System.out.println("---ToastLoad--- Opened");
        Cursor cursor = sdao.mDb.rawQuery("select * from " + TABLE_NAME, new String[]{});
        if(cursor.moveToFirst()) {
            UUID idslime = UUID.fromString(cursor.getString(0));
            System.out.println("---ToastLoad--- InFirstRow : " + idslime.toString());
            String name = cursor.getString(1);
            long lastco = cursor.getLong(2);
            double barfaim = cursor.getDouble(3);
            double barjoie = cursor.getDouble(4);
            Slime s = new Slime(idslime, name, lastco, barfaim, barjoie);
            SlimeApplication.setCurrentSlime(s);
            cursor.close();
            sdao.close();
        }else{ System.out.println("---ToastLoad--- Null"); cursor.close(); sdao.close(); return false; }
        return true;
    }

    public static void saveSlime(Context c, Slime s){
        SlimeDAO sdao = new SlimeDAO(c);
        sdao.open();
        if(sdao.isInDB(s)) sdao.sauvegarder(s); else sdao.creer(s);
        sdao.close();
    }

    public boolean isInDB(Slime s) {
        return charger(s.getId())!= null;
    }

    public static Slime loadSlime(Context c, UUID id){
        SlimeDAO sdao = new SlimeDAO(c);
        sdao.open();
        Slime s = sdao.charger(id);
        sdao.close();
        return s;
    }

    public static List<Pair<String, UUID>> getSlimeList(Context c){
        SlimeDAO sdao = new SlimeDAO(c);
        sdao.open();

        List<Pair<String, UUID>> slimeList = new ArrayList<>();
        Cursor cursor = sdao.mDb.rawQuery("SELECT "+KEY+", "+NAME+" FROM "+TABLE_NAME , new String[]{});
        while(cursor.moveToNext()){
            slimeList.add( new Pair<>(
                    cursor.getString(1),
                    UUID.fromString(cursor.getString(0))
            ));
        }
        cursor.close();
        sdao.close();
        return slimeList;
    }

    public static List<SlimeDB> getSlimesStatus(Context c){
        SlimeDAO sdao = new SlimeDAO(c);
        sdao.open();
        List<SlimeDB> slimeList = new ArrayList<>();
        Cursor cursor = sdao.mDb.rawQuery("SELECT * FROM " + TABLE_NAME , new String[]{});
        while(cursor.moveToNext()){
            slimeList.add(new SlimeDB( cursor.getString(1), cursor.getDouble(3),cursor.getDouble(4), cursor.getLong(2)));
        }
        cursor.close();
        sdao.close();
        return slimeList;
    }

    public static class SlimeDB{
        String name;
        double bf, bj;
        long lc;
        public SlimeDB(String nm, double bf, double bj, long lc){
            this.name = nm;
            this.bf = bf;
            this.bj = bj;
            this.lc = lc;
        }

        public String getName() {
            return name;
    }

        public double getBf() {
            return bf;
        }

        public double getBj() {
            return bj;
        }

        public long getLc() {
            return lc;
        }
    }

    public static void wipeContent(Context c) {
        SlimeDAO sdao = new SlimeDAO(c);
        sdao.open();
        sdao.mDb.execSQL(TABLE_DROP);
        sdao.mDb.execSQL(TABLE_CREATE);
        sdao.close();
    }
}






