package fr.amdayopa.slime;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import fr.amdayopa.slime.database.SlimeDAO;
import fr.amdayopa.slime.graphics.shapes.Hitbox;
import fr.amdayopa.slime.physics.FoodObject;
import fr.amdayopa.slime.slimeBoi.FoodType;
import fr.amdayopa.slime.slimeBoi.Scene;
import fr.amdayopa.slime.slimeBoi.Slime;
import fr.amdayopa.slime.utils.Pair;

public class StartActivity extends AppCompatActivity {

    private Scene scene;
    private DrawerLayout mDrawerLayout;

    private static AudioAttributes attrs;
    private static SoundPool sp;
    private static int soundIds[];
    private  static HashMap<Integer, UUID> slimes = new HashMap<>();

    private static int width;
    private static int height;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        attrs = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        sp = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(attrs)
                .build();

        soundIds = new int[10];
        soundIds[0] = sp.load(getApplicationContext(), R.raw.nice, 1);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        setContentView(R.layout.startactivity);

        //Remplacement de la toolbar par notre toolbar personalisé
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }

        //Récuperation de la taille de l'écran et stockage de celle-ci dans une variable static
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        //Creation de la scene
        scene = new Scene(
                getApplicationContext(),
                (RelativeLayout) findViewById(R.id.rl1)
        );

        SlimeApplication.getCurrentSlime().setInScene(scene);
        this.setTitle(SlimeApplication.getCurrentSlime().getName());

        mDrawerLayout = findViewById(R.id.drawerL);

        // Boutons de tests
        final ImageButton button = findViewById(R.id.button_1);
        final ImageButton button3 = findViewById(R.id.button_3);
        final ImageButton button4 = findViewById(R.id.button_4);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                scene.interactionMode = "grab";
                new FoodObject(scene, FoodObject.getCurrentType());
            }
        });
        button.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                dispatchTakePictureIntent();
                return true;
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                scene.interactionMode = "grab";
                button3.setImageResource(R.drawable.grab_button_active);
                button4.setImageResource(R.drawable.button_pet);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                scene.interactionMode = "pet";
                button4.setImageResource(R.drawable.pet_button_active);
                button3.setImageResource(R.drawable.button_grab);
            }
        });

        final NavigationView navigationView = findViewById(R.id.nav_view);
        setNavigationViewItem(navigationView);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                        Menu menu = navigationView.getMenu();

                        if(menu.findItem(R.id.slimelist).getSubMenu().findItem(menuItem.getItemId()) != null ) {
                            updateItemsOnSelected(menu, menuItem.getOrder());
                            changeSlime(menuItem.getOrder());
                        }else{
                            switch (menuItem.getItemId()){
                                case R.id.destroyAllSlime:
                                    SlimeDAO.wipeContent(getApplicationContext());
                                case R.id.createSlime:
                                    Intent intent1 = new Intent(StartActivity.this, SlimeCActivity.class);
                                    startActivity(intent1);
                                    finish();
                            }
                            mDrawerLayout.closeDrawers();
                        }
                        return true;
                    }
                });

    }

    private void changeSlime(int order) {
        Slime s = SlimeDAO.loadSlime(getApplicationContext(), slimes.get(order));
        SlimeApplication.setCurrentSlime(s);
        Intent intent1 = new Intent(StartActivity.this, StartActivity.class);
        startActivity(intent1);
        finish();
    }

    private void setNavigationViewItem(NavigationView navigationView) {
        MenuItem slimeList = navigationView.getMenu().findItem(R.id.slimelist);
        List<Pair<String, UUID>> slimeNames = SlimeDAO.getSlimeList(getApplicationContext());
        for (int i = 0; i<slimeNames.size(); i++ ) {
            slimeList.getSubMenu()
                    .add(Menu.NONE, 100+i, i, slimeNames.get(i).getL())
                    .setIcon(R.drawable.body).setChecked(slimeNames.get(i).getR().equals(SlimeApplication.getCurrentSlime().getId()));
            slimes.put(i, slimeNames.get(i).getR());
        }
    }
    private void updateItemsOnSelected(Menu menu, int selectedPosition) {
        MenuItem menuItem = menu.findItem(R.id.slimelist);
        SubMenu subMenu = menuItem.getSubMenu();

        for(int i = 0; i < subMenu.size(); i++){
            MenuItem item = subMenu.getItem(i);
            item.setChecked(i == selectedPosition);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    private void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.nav_view, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.toggleHitbox:
                Hitbox.toggleVisibility(scene);
                item.setChecked(Hitbox.isVisibile());
                return true;
            case R.id.changehfactor:
                Slime.toggleHF();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap photoBitmap = (Bitmap) extras.get("data");
            int hue = scene.averageColor(photoBitmap);
            FoodObject.setCurrentType(FoodType.getClosestType(hue));
            new FoodObject(scene, FoodObject.getCurrentType());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(SlimeApplication.getCurrentSlime() != null)
            SlimeDAO.saveSlime(getApplication(),SlimeApplication.getCurrentSlime());
    }

    public static int getWidth() {return width;}
    public static int getHeight() {return height;}

    public static void playNoice(float rate){
        sp.play(soundIds[0], 1, 1, 1, 0, rate);
    }
}