package fr.amdayopa.slime.graphics;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import fr.amdayopa.slime.slimeBoi.Scene;

public abstract class Graphic{
    // Nom de l'image
    String content;

    // Imageview associée (générée par la méthode show) --> à mettre dans le constructeur éventuellement)
    public ImageView iv;
    //-----------------------------------------------------------------------------------------------

    // Propriétés absolues
    float pos_X=0;
    float pos_Y=0;

    float angle=0;

    private float size_X=1;
    private float size_Y=1;

    float alpha = 1;

    Scene scene;
    protected boolean graphicVisibility = true;
    // --------------------

    /*
            public Point getTouchPositionFromDragEvent(View item, DragEvent event) {
                Rect rItem = new Rect();
                item.getGlobalVisibleRect(rItem);
                return new Point(rItem.left + Math.round(event.getX()), rItem.top + Math.round(event.getY()));
            }
    */
    public float getX(){return pos_X;}
    public float getY(){return pos_Y;}



    public void destroy(){
        scene.getRl().removeView(iv);
    }

    @Deprecated
    public void makeDraggable() {
        iv.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_MOVE:
                        move(event.getRawX(), event.getRawY()-100);
                        //FunFair.print(Float.toString(event.getRawX()), iv);
                        break;
                }
                return true;
            }
        });
    }

    public boolean isHit(Graphic target){
        int pos_X1=(int) pos_X;
        int pos_Y1=(int) pos_Y;
        int width1=(int) iv.getWidth();
        int height1=(int) iv.getHeight();

        int pos_X2=(int) target.pos_X;
        int pos_Y2=(int) target.pos_Y;
        int width2=(int) target.iv.getWidth();
        int height2=(int) target.iv.getHeight();

        int z_intex;

        Rect rect1 = new Rect(pos_X1,pos_Y1,pos_X1+width1,pos_Y1+height1);
        Rect rect2 = new Rect(pos_X2,pos_Y2,pos_X2+width2,pos_Y2+height2);

        return rect1.intersect(rect2);
    }

    public void Index(int z){
        iv.setZ(z);
    }


    public void move(float mpos_X,float mpos_Y){
        pos_X = mpos_X;
        pos_Y = mpos_Y;

        iv.setX(pos_X);
        iv.setY(pos_Y);
    }

    public void move(final float mpos_X,final float mpos_Y,final int delay){

        ObjectAnimator animX= ObjectAnimator.ofFloat(iv, "translationX", pos_X, mpos_X);
        ObjectAnimator animY= ObjectAnimator.ofFloat(iv, "translationY", pos_Y, mpos_Y);
        animX.setRepeatCount(0);
        animY.setRepeatCount(0);

        animX.setDuration(delay);
        animY.setDuration(delay);

        animX.start();
        animY.start();

        animX.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                pos_X = (Float)animation.getAnimatedValue();
                //FunFair.print(Float.toString(mpos_X), iv);
            }
        });
        animY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                pos_Y = (Float)animation.getAnimatedValue();
            }
        });
    }

    public void rotate(float mangle){
        angle = mangle;
        iv.setRotation(angle);
    }

    public void rotate(final float mangle,final int delay){

        ObjectAnimator anim = ObjectAnimator.ofFloat(iv, "rotation", angle, mangle);
        anim.setRepeatCount(0);
        anim.setDuration(delay);
        anim.start();

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                angle = (Float)animation.getAnimatedValue();
                //print(Float.toString(r));
            }
        });
    }

    public void scale(float msize_X , float msize_Y){
        size_X = msize_X;
        size_Y = msize_Y;
        iv.setScaleX(msize_X);
        iv.setScaleY(msize_Y);
    }

    public void scale(final float msize_X,final float msize_Y, final int delay){
        ObjectAnimator animX= ObjectAnimator.ofFloat(iv, "scaleX", size_Y, msize_X);
        ObjectAnimator animY= ObjectAnimator.ofFloat(iv, "scaleY", size_Y, msize_Y);
        animX.setRepeatCount(0);
        animY.setRepeatCount(0);

        animX.setDuration(delay);
        animY.setDuration(delay);

        animX.start();
        animY.start();

        animX.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                size_X = (Float)animation.getAnimatedValue();
                //print(Float.toString(x));
            }
        });
        animY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                size_Y = (Float)animation.getAnimatedValue();
            }
        });
    }
    public void setAlpha(float mAlpha){
        iv.setAlpha(mAlpha);
    }

    public void toggleTransparancy(){
        if(graphicVisibility){
            setAlpha(0);
            graphicVisibility = false;
        }else{
            setAlpha(alpha);
            graphicVisibility = true;
        }
    }

    public float getSize_X() {
        return size_X;
    }

    public float getSize_Y() {
        return size_Y;
    }
}
