package fr.amdayopa.slime.graphics;

import android.widget.ImageView;

import fr.amdayopa.slime.slimeBoi.Scene;

public class Image extends Graphic{
    public Image(String c , Scene mScene){
        content = c;

        iv = new ImageView(mScene.getContext());
        iv.setImageResource(iv.getResources().getIdentifier(
                content,                                    //Nom du drawable lié à l'objet
                "drawable", mScene.getContext().getPackageName())       //drawable / drawable-nodpi
        );


        mScene.getRl().addView(iv);
    }
}