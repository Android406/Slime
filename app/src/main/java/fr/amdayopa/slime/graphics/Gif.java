package fr.amdayopa.slime.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.constraint.solver.LinearSystem;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import fr.amdayopa.slime.slimeBoi.Scene;


public class Gif extends Graphic{

    // Array of frames
    private Bitmap[] bmps;
    // Number of frames
    private int fNumber;
    // fps and total duration
    private static final int fduration = 1000/30;
    public int duration;

    //Drawing attributes
    private AnimationDrawable animation;
    private boolean Brepeat = true;

    Canvas canvas;
    public int width  = 0;
    public int height = 0;

    bmpThread thread;
    class bmpThread extends Thread {
        private int color = 0;
        @Override
        public void run(){
            for(Layer layer : layerList) {
                for (int i = 0; i < fNumber; i++) {
                    Log.d("file", content + String.format("%05d", i));
                    Drawable rawDrawable = scene.getContext().getResources().getDrawable(scene.getContext().getResources().getIdentifier(
                            layer.content + String.format("%05d", i),
                            "drawable", scene.getContext().getPackageName()));

                    if (layer.color != 0) {
                        rawDrawable.setColorFilter(layer.color, PorterDuff.Mode.MULTIPLY);
                    }

                    Canvas canvas = new Canvas(bmps[i]);

                    rawDrawable.setBounds(0, 0, width, canvas.getHeight());
                    rawDrawable.draw(canvas);
                }
                this.interrupt();
            }
        }
    }

    ArrayList<Layer> layerList;
    class Layer{
        public String content;
        public int color;

        public Layer(String mContent, int mColor){
            content = mContent;
            color = mColor;
        }
    }

    public Gif(int mWidth, int mHeight, int mfNumber,Scene mScene) {
        scene = mScene;
        iv = new ImageView(scene.getContext());
        fNumber = mfNumber;
        bmps = new Bitmap[fNumber];

        width = mWidth;
        height = mHeight;

        // Initialising bmp array
        for(int i = 0; i < fNumber; i++){
            bmps[i] = Bitmap.createBitmap(
                    width, height,
                    Bitmap.Config.ARGB_8888);
        }

        thread = new bmpThread();
        layerList = new ArrayList<Layer>();

        scene.getRl().addView(iv);
    }

    public void addLayer(String mContent, int mColor){
        layerList.add(new Layer(mContent,mColor));
    }

    public void generate(){
        thread.start();
    }

    public void play(int start, int end){
        animation = new AnimationDrawable();

        for (int i=start; i != end; i+=(start<end?1:-1)) {
            animation.addFrame(new BitmapDrawable(scene.getContext().getResources(), bmps[i]), fduration);
        }

        animation.setOneShot(Brepeat);
        duration = (end-start)*fduration;
        iv.setBackground(animation);
        animation.start();
    }

    public void setFrame(int frame){
        iv.setBackground(new BitmapDrawable(scene.getContext().getResources(),bmps[frame]));
    }

    public void repeat(boolean mBool){
        Brepeat = mBool;
    }
    public void stop(){
        animation.stop();
    }

    @Override
    public void destroy(){
        super.destroy();
        for(Bitmap bmp :bmps){
            bmp.recycle();
            bmp = null;
        }

        for(Layer layer : layerList){
            layerList.remove(layer);
        }
    }
}