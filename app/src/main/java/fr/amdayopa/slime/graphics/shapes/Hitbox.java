package fr.amdayopa.slime.graphics.shapes;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;

import fr.amdayopa.slime.physics.PhysicObject;
import fr.amdayopa.slime.slimeBoi.Scene;

public class Hitbox extends Shape{

    private static boolean visibility = false;

    public Hitbox(Scene mScene, int y, int x) {
        super(mScene, y,x);
    }

    @Override
    public void drawBitMap() {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        if(!visibility) return;

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(4);
        paint.setStyle(Paint.Style.STROKE);

        canvas.drawColor(Color.alpha(Color.TRANSPARENT));
        canvas.drawRect(0, 0, width, height, paint);
        paint.setStrokeWidth(2);
        canvas.drawLine(0, 0, width, height, paint);
        canvas.drawLine(width, 0, 0, height, paint);
    }

    public static void toggleVisibility(Scene scene) {
        visibility=!visibility;
        for (PhysicObject physicObject : scene.ObjectList) {
            physicObject.getHitbox().drawBitMap();
        }
    }

    public static boolean isVisibile() {
        return visibility;
    }
}
