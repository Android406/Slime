package fr.amdayopa.slime.graphics.shapes;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.ImageView;

import fr.amdayopa.slime.graphics.Graphic;
import fr.amdayopa.slime.slimeBoi.Scene;

public abstract class Shape extends Graphic {
    protected int height;
    protected int width;
    protected  Bitmap bitmap;
    protected Paint paint;
    protected  Scene scene;
    protected Canvas canvas;

    public Shape(Scene mScene, int y, int x) {
        height = y;
        width = x;
        scene = mScene;
        bitmap = Bitmap.createBitmap(
                width,
                height,
                Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        drawBitMap();
        iv = new ImageView(mScene.getContext());
        iv.setImageBitmap(bitmap);
        scene.getRl().addView(iv);
    }

    public abstract void drawBitMap();
}
