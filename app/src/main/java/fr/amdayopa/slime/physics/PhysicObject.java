package fr.amdayopa.slime.physics;

import android.graphics.Rect;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import fr.amdayopa.slime.StartActivity;
import fr.amdayopa.slime.graphics.Graphic;
import fr.amdayopa.slime.graphics.shapes.Hitbox;
import fr.amdayopa.slime.graphics.shapes.Shape;
import fr.amdayopa.slime.slimeBoi.Scene;

public class PhysicObject{
    public float pos_X = 0;
    public float pos_Y = 0;
    public float last_pos_X = 0;
    public float last_pos_Y = 0;

    float pos_Z = 0;

    public int height=100;
    public int width=100;

    final int ground = (int) (StartActivity.getHeight()*0.7);

    protected Scene scene;

    PhysicType type = PhysicType.NONE;

    ArrayList<BoundGraphic> graphics;
    public ArrayList<PhysicObject> CollisionList;
    int graphicListID = 0;

    private Hitbox hitbox;

    //runs without a timer by reposting this handler at the end of the runnable
    Handler timerHandler = new Handler();
    long startTime = 0;

    int g = 9*10;
    double gravity = 0.5;
    double bounciness = 3;

    long fallTime = 0;
    int start_Z;
    String state = "none";
    protected Vector2D velocity = new Vector2D();

    public PhysicObject(){}

    public PhysicObject(Scene mScene, PhysicType type){
        this(mScene);
        this.type = type;
    }

    public PhysicObject(Scene mScene){
        graphics = new ArrayList<BoundGraphic>();
        CollisionList = new ArrayList<PhysicObject>();

        fallTime = System.currentTimeMillis();
        start_Z = Math.round(pos_Y);
        scene = mScene;

        setHitbox(width, height);       //Hitbox par défaut
        scene.addObject(this);  //Ajout de l'objet à la liste des objets physiques de la scène



        startTime = System.currentTimeMillis();
        // On démarre le timer :
        timerHandler.postDelayed(timerRunnable, 0);
        //On arrête le timer :
        //timerHandler.removeCallbacks(timerRunnable);
    }

    //Destructeur
    public void destroy(){
        deleteAllGraphics();
        deleteHitbox();
        scene.ObjectList.remove(this);
    }

    public Hitbox getHitbox() {
        return hitbox;
    }

    public void setHitbox(Hitbox hitbox) {
        this.hitbox = hitbox;
    }

    //---------------------------Bound Graphics-------------------------//
    protected class BoundGraphic{
        public int relative_X = 0;
        public int relative_Y = 0;

        public float relative_size_X=1;
        public float relative_size_Y=1;

        public Graphic graphic;
        public int ID;

        public BoundGraphic(Graphic mGraphic, int mID){
            graphic = mGraphic;
            relative_X = (int)graphic.getX();
            relative_Y = (int)graphic.getY();
            relative_size_X = graphic.getSize_X();
            relative_size_Y = graphic.getSize_Y();
            ID = mID;
        }
        public ImageView getview(){
            return(graphic.iv);
        }
    }

    public int addGraphic(Graphic mGraphic){
        graphics.add(new BoundGraphic(mGraphic,graphicListID));
        graphicListID++;
        return(graphicListID-1);
    }

    public void replaceGraphic(Graphic mGraphic, int mID){
        for(BoundGraphic bGraphic : graphics){
            if(bGraphic.ID == mID){
                bGraphic.graphic = mGraphic;
            }
        }
    }

    private void updateGraphics(){
        for(BoundGraphic bGraphic : graphics){
            int relative_X = bGraphic.relative_X;
            int relative_Y = bGraphic.relative_Y;

            bGraphic.graphic.move(
                    pos_X+relative_X,
                    pos_Y+relative_Y);
//            Vector2D scaleVect = new Vector2D(velocity);
//            scaleVect = scaleVect.normalize();
//            bGraphic.graphic.scale((float)scaleVect.getX() + 1,(float)scaleVect.getY() + 1);
            bGraphic.graphic.scale(
                    bGraphic.relative_size_X*((float)(Math.abs(velocity.getX())/100)+1),
                    bGraphic.relative_size_Y*((float)(velocity.getY()/100)+1)
            );
        }
    }

    private void deleteAllGraphics(){
        for(BoundGraphic bGraphic : graphics) {
            deleteGraphic(bGraphic);
        }
    }

    private void deleteGraphic(BoundGraphic bGraphic){
        bGraphic.graphic.destroy(); //On détruit l'imageview contenue par le boundgraphic
        graphics.remove(bGraphic);  //On retire le boundgraphic de la liste des graphics
    }

    public void setHitbox(int mWidth, int mHeight){
        height = mHeight;
        width = mWidth;

        if(hitbox != null) {
            scene.getRl().removeView(hitbox.iv);
        }
        hitbox = new Hitbox(scene,height,width);

        hitbox.iv.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        if(scene.interactionMode == "grab") {
                            hitbox.move(event.getRawX() - width / 2, event.getRawY() - 205 - height / 2);
                            pos_X = hitbox.getX();
                            pos_Y = hitbox.getY();
                            last_pos_X = pos_X;
                            last_pos_Y = pos_Y;
                            updateGraphics();

                            state = "held";
                        }
                        RunOnDOWN(event);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(scene.interactionMode == "grab") {
                            pos_X = event.getRawX() - width / 2;
                            pos_Y = event.getRawY() - 205 - height / 2;

                            velocity.setX((pos_X - last_pos_X) / 2);
                            velocity.setY((pos_Y - last_pos_Y) / 2);

                            last_pos_X = pos_X;
                            last_pos_Y = pos_Y;

                            checkWallCollision();
                            hitbox.move(pos_X, pos_Y);
                            updateGraphics();

                            start_Z = Math.round(pos_Y);
                        }
                        RunOnMOVE(event);
                        break;
                    case MotionEvent.ACTION_UP:
                        if(scene.interactionMode == "grab") {
                            state = "none";
                            fallTime = System.currentTimeMillis();
                            //velocity = new Vector2D();
                        }
                        RunOnUP(event);
                        break;
                }

                return true;
            }
        });
    }

    public void RunOnDOWN(MotionEvent event){}
    public void RunOnUP(MotionEvent event){}
    public void RunOnMOVE(MotionEvent event){}

    public void deleteHitbox(){
        scene.getRl().removeView(hitbox.iv);
        //hitbox = null;
    }

    public void setHiboxFromLastGraphic(){

    }

    //------------------------------Timer-------------------------------//
    // Tourne tant que l'objet existe                                   //
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            // Tick actuel (commence à 0)
            long millis = System.currentTimeMillis() - startTime;
            updateGraphics();

            //Toutes les méthodes à effectuer à chaque Tick//
            if(state == "none"){
//                fall(System.currentTimeMillis() - fallTime,start_Z);
                fall();
            }

            setCollisionList();
            RunOnTick();
            //---------------------------------------------//

            // Tick suivant :
            timerHandler.postDelayed(this, 10);
        }
    };
    //--------------------------Physic methods--------------------------//
    // Chute -> Changer en méthode "force"/"accélération"

    @Deprecated
    public void fall(long t, int z){
        if(pos_Y<ground-height){
            pos_Y+=g*t/1000;
            if(pos_Y>ground-height){
                pos_Y=ground-height;
            }
            //pos_Y = 1/2*g*t*t+z;
        }else{
            pos_Y=ground-height;
        }
        hitbox.move(pos_X,pos_Y);
    }
    
    public void fall(){
        velocity.add(0,gravity);
        pos_X += velocity.getX();
        pos_Y += velocity.getY();
        checkWallCollision();
        hitbox.move(pos_X, pos_Y);
    }

    public void checkWallCollision(){

        if( pos_Y >= ground - height ) {
            pos_Y = ground - height;
            velocity.setX(velocity.getX()/bounciness);
            velocity.setY(-velocity.getY()/bounciness);
        }
        if( pos_X < 0 ){
            pos_X = 0;
            velocity.setX(-velocity.getX()/bounciness);
        }
        if( pos_X > StartActivity.getWidth()-width){
            pos_X = StartActivity.getWidth()-width;
            velocity.setX((-velocity.getX()/bounciness));
        }
    }

    public void RunOnTick(){};

    public boolean isHit(PhysicObject target){
        int pos_X1=(int) pos_X;
        int pos_Y1=(int) pos_Y;
        int width1=width;
        int height1=height;

        int pos_X2=(int) target.pos_X;
        int pos_Y2=(int) target.pos_Y;
        int width2=target.width;
        int height2=target.height;

        int z_intex;

        Rect rect1 = new Rect(pos_X1,pos_Y1,pos_X1+width1,pos_Y1+height1);
        Rect rect2 = new Rect(pos_X2,pos_Y2,pos_X2+width2,pos_Y2+height2);

        return rect1.intersect(rect2);
    }

    // Construction la liste de tous les objects en collision avec celui-ci (INUTILE)

    public void setCollisionList(){
        CollisionList.clear();
        for(PhysicObject pObject : scene.ObjectList){
            if(isHit(pObject)){
                CollisionList.add(pObject);
            }
        }
    }

}
