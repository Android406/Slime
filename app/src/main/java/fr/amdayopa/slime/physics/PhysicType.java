package fr.amdayopa.slime.physics;

public enum PhysicType {
    FOOND("Foond"),
    SLIME("Slime"),
    NONE("Undefined");

    private String name;

    PhysicType(String name){
        this.setName(name);
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
