package fr.amdayopa.slime.physics;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;

import java.io.IOException;
import java.util.Random;

import fr.amdayopa.slime.MenuActivity;
import fr.amdayopa.slime.R;
import fr.amdayopa.slime.SlimeApplication;
import fr.amdayopa.slime.graphics.Gif;
import fr.amdayopa.slime.graphics.Image;
import fr.amdayopa.slime.slimeBoi.Scene;
import fr.amdayopa.slime.slimeBoi.Slime;

import static fr.amdayopa.slime.StartActivity.playNoice;

public class SlimeObject extends PhysicObject{
    Gif slimeGif;
//  MediaPlayer eatSound = MediaPlayer.create(Scene.getContext(), R.raw.nice);

    int faceID;
    int bodyID;
    String animState = "idle";
    int animDuration=0;

    public SlimeObject(Scene mScene, int color){
        super(mScene, PhysicType.SLIME);

        slimeGif = new Gif(210,175, 302, mScene);
        slimeGif.addLayer("slime_body", color);
        slimeGif.addLayer("slime_face",0);
        slimeGif.generate();

        slimeGif.scale(2,2);
        slimeGif.move(100,80);
        this.addGraphic(slimeGif);

        slimeGif.repeat(false);
        slimeGif.play(0,60);

        setHitbox(420,350);

//        eatSound.setVolume(3,3);
    }

    @Override
    public void RunOnMOVE(MotionEvent event){
        if(scene.interactionMode == "pet") {
            int xFrame = Math.round(273 + event.getX()/420*29);
            if(xFrame<273){
                xFrame=273;
            }else if(xFrame>301){
                xFrame=301;
            }
            slimeGif.setFrame(xFrame);
            animState = "petting";
            SlimeApplication.getCurrentSlime().pet();
        }
    }

    @Override
    public void RunOnUP(MotionEvent event){
        if(scene.interactionMode == "pet") {
            slimeGif.repeat(false);
            slimeGif.play(0,60);
            animState = "idle";
        }
    }

    @Override
    public void RunOnTick() {
        super.RunOnTick();

        animDuration-=10;

        if(animState=="eating" && animDuration<=0){

            slimeGif.repeat(false);
            slimeGif.play(0,60);
            animState = "idle";
        }

        for(PhysicObject pObject : CollisionList){
            if(pObject.type == PhysicType.FOOND){
                pObject.destroy();

                animDuration = 300*33;
                animState = "eating";


                slimeGif.repeat(true);
                slimeGif.play(120,270);

                SlimeApplication.getCurrentSlime().feed(0.1);
                playNoice((float) (1+Math.random()));
            }
        }
    }
}
