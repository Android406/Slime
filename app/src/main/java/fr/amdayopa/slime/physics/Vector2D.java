package fr.amdayopa.slime.physics;

import java.lang.Math;

public class Vector2D {

    protected double dX;
    protected double dY;

    public Vector2D() {
        dX = dY = 0.0;
    }

    public Vector2D( double dX, double dY ) {
        this.dX = dX;
        this.dY = dY;
    }

    public Vector2D(Vector2D velocity) {
        this.dX = velocity.dX;
        this.dY = velocity.dY;
    }

    public String toString() {
        return "Vector2D(" + dX + ", " + dY + ")";
    }

    public double length() {
        return Math.sqrt ( dX*dX + dY*dY );
    }

    public double lengthSquared() {
        return dX*dX + dY*dY;
    }

    public Vector2D add( Vector2D v1 ) {
        Vector2D v2 = new Vector2D( this.dX + v1.dX, this.dY + v1.dY );
        return v2;
    }

    public Vector2D sub( Vector2D v1 ) {
        Vector2D v2 = new Vector2D( this.dX - v1.dX, this.dY - v1.dY );
        return v2;
    }

    public Vector2D scale( double scaleFactor ) {
        Vector2D v2 = new Vector2D( this.dX*scaleFactor, this.dY*scaleFactor );
        return v2;
    }

    public void add(int x, double y) {
        this.dX += x;
        this.dY += y;
    }

    public void add(double i) {
        this.dX += i;
        this.dY += i;
    }
    public Vector2D normalize() {
        Vector2D v2 = new Vector2D();

        double length = Math.sqrt( this.dX*this.dX + this.dY*this.dY );
        if (length != 0) {
            v2.dX = this.dX/length;
            v2.dY = this.dY/length;
        }
        return v2;
    }

    public double getX() {
        return dX;
    }
    public void setX(double x) {
        this.dX = x;
    }

    public double getY() {
        return dY;
    }

    public void setY(double y) {
        this.dY = y;
    }
}