package fr.amdayopa.slime.physics;

import android.widget.ImageView;
import android.widget.TextView;

import fr.amdayopa.slime.R;
import fr.amdayopa.slime.graphics.Gif;
import fr.amdayopa.slime.graphics.Graphic;
import fr.amdayopa.slime.graphics.Image;
import fr.amdayopa.slime.slimeBoi.FoodType;
import fr.amdayopa.slime.slimeBoi.Scene;

public class FoodObject extends PhysicObject{

    private static FoodType currentType = FoodType.RED;

    public FoodObject(Scene mScene, FoodType type){
        super(mScene, PhysicType.FOOND);
        String imgSrc = type.getSrc();
        ImageView iv = Scene.getRl().findViewById(R.id.selectedFoodImage);
        iv.setImageResource(iv.getResources().getIdentifier(imgSrc, "drawable", mScene.getContext().getPackageName()));
        ((TextView)Scene.getRl().findViewById(R.id.textColor)).setText(imgSrc);

        this.addGraphic(new Image(imgSrc, mScene));
        velocity = new Vector2D(Math.random()*12,0);
        pos_Y=100;
        setHitbox(graphics.get(0).getview().getDrawable().getIntrinsicWidth(), graphics.get(0).getview().getDrawable().getIntrinsicHeight());
    }

    public static FoodType getCurrentType() {
        return currentType;
    }

    public static void setCurrentType(FoodType currentType) {
        FoodObject.currentType = currentType;
    }

    @Override
    public void RunOnTick() {
        super.RunOnTick();
    }
}
