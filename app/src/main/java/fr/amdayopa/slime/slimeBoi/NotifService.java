package fr.amdayopa.slime.slimeBoi;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.List;

import fr.amdayopa.slime.MenuActivity;
import fr.amdayopa.slime.SlimeApplication;
import fr.amdayopa.slime.database.SlimeDAO;
import fr.amdayopa.slime.utils.Pair;

import static fr.amdayopa.slime.utils.Funfest.notif;

/**
 * Created by Dalil on 26/03/2018.
 */

public class NotifService extends IntentService{
    public NotifService() {
        super("notifService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(SlimeApplication.getCurrentSlime() != null)
            SlimeDAO.saveSlime(getApplication(),SlimeApplication.getCurrentSlime());
        Context ctx = getApplicationContext();
        List<SlimeDAO.SlimeDB> slimelist = SlimeDAO.getSlimesStatus(ctx);
        int i = 0;
        for(SlimeDAO.SlimeDB value : slimelist){
            boolean faim = (value.getBf()-((Calendar.getInstance().getTime().getTime()-value.getLc())/Slime.hFactor)<0.1),
                    joie = (value.getBj()-((Calendar.getInstance().getTime().getTime()-value.getLc())/Slime.hFactor)<0.1),
                    mort = (value.getBj()-((Calendar.getInstance().getTime().getTime()-value.getLc())/Slime.hFactor)<=0.0) &&
                            (value.getBf()-((Calendar.getInstance().getTime().getTime()-value.getLc())/Slime.hFactor)<=0.0);
            if((faim || joie)){
                notif("Alert SlimeBoi",value.getName()+" "+(mort ? "est mort.": (faim?"a faim ":"")+(faim && joie ?"et ":"")+(joie?"s'ennuie.":"."))
                        , MenuActivity.class, ctx, i);

            }
            i++;
        }
    }
}
