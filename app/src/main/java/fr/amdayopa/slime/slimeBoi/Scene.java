package fr.amdayopa.slime.slimeBoi;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Color;
import android.support.v7.view.menu.BaseMenuPresenter;
import android.util.Log;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import fr.amdayopa.slime.physics.PhysicObject;

public class Scene {
    protected Slime slime;
    protected SceneState state;
    public ArrayList<PhysicObject> ObjectList;

    static private RelativeLayout rl;
    static private Context context;
    static public String interactionMode = "grab";


    public Scene(Context mContext, RelativeLayout mLayout){
        context = mContext;
        rl = mLayout;
        ObjectList = new ArrayList<PhysicObject>();
    }
    public void addObject(PhysicObject mPhysic){
        ObjectList.add(mPhysic);
    }
    static public RelativeLayout getRl() {
        return rl;
    }
    static public Context getContext() {
        return context;
    }

    public int averageColor(Bitmap bmp){
        /*bmp =  Bitmap.createBitmap(
                BitmapFactory.decodeResource(getContext().getResources(),
                        getContext().getResources().getIdentifier(
                                "orange",
                                "drawable", getContext().getPackageName())
                ));*/

        int width = bmp.getWidth();
        int height = bmp.getHeight();

        int hue=0;
        float[] hsv = new float[3];
        int red = 0;
        int green = 0;
        int blue = 0;
        int pixelNumber = 0;
        for(int x=0; x<width;x++){
            for(int y=0; y<height;y++){
                int color = bmp.getPixel(x,y);
                String hexColor = Integer.toHexString(color);

                Color.RGBToHSV(
                        (int) Integer.valueOf( hexColor.substring( 2, 4 ), 16 ),
                        (int )Integer.valueOf( hexColor.substring( 4, 6 ), 16 ),
                        (int) Integer.valueOf( hexColor.substring( 6, 8 ), 16 ),
                        hsv);

                if(hsv[1]>0.5) {
                    Log.d("color",
                            Integer.toString((int) Integer.valueOf(hexColor.substring(2, 4), 16)) + " " +
                                    Integer.toString((int) Integer.valueOf(hexColor.substring(4, 6), 16)) + " " +
                                    Integer.toString((int) Integer.valueOf(hexColor.substring(6, 8), 16))
                    );
                    hexColor = Integer.toHexString(Color.HSVToColor(new float[]{hsv[0], 1F, 1F}));
                    Log.d("color final", hexColor);

                    red += Integer.valueOf(hexColor.substring(2, 4), 16);
                    green += Integer.valueOf(hexColor.substring(4, 6), 16);
                    blue += Integer.valueOf(hexColor.substring(6, 8), 16);

                    pixelNumber++;
                }
            }
        }
        red = red/pixelNumber;
        green = green/pixelNumber;
        blue = blue/pixelNumber;

        Color.RGBToHSV(red,green,blue,hsv);
        return Math.round(hsv[0]);
    }
}
