package fr.amdayopa.slime.slimeBoi;

import android.graphics.Color;
import android.util.Log;

import java.util.Calendar;
import java.util.UUID;

import fr.amdayopa.slime.physics.PhysicObject;
import fr.amdayopa.slime.physics.SlimeObject;
import fr.amdayopa.slime.utils.Funfest;

public class Slime{
    private UUID id;
    private String name;
    private int color = Color.GREEN;

    private long lastConnection;
    private double barFaim; //barre de faim
    private double barJoie;
    private SlimeObject mSlimeObject;
    public static double hFactor = 600000;

    public Slime() { //Constructeur par défaut
        id = UUID.randomUUID();
        barFaim = 1.0;
        barJoie = 0.5;
        lastConnection = Calendar.getInstance().getTime().getTime();
    }

    public void setInScene(Scene mScene){
        mSlimeObject = new SlimeObject(mScene,color);
    }

    public Slime(double barFaim, long lastConnection){
        this.barFaim = barFaim;
        this.lastConnection = lastConnection;
        faim();
    }

    public void feed(double foodValue){
        barFaim += foodValue;
        barFaim = (barFaim>1?1:barFaim);
    }

    //Constructeur par initialisation lancé par la bdd
    public Slime(UUID id, String name, long lastco, double barfaim, double barJoie) {
        this.id = id;
        this.name = name;
        this.lastConnection = lastco;
        this.barFaim = barfaim;
        this.barJoie = barJoie;

        color = Color.HSVToColor( new float[] {(float)Funfest.randomGenerator(Funfest.stringToSeed(name))*360.0f,1.0f,1.0f}  );
    }

    public boolean faim(){
        barFaim -= ((Calendar.getInstance().getTime().getTime()-lastConnection)/hFactor);
        barFaim = (barFaim<0?0:barFaim);
        return (barFaim >= 0);
    }

    public boolean joie(){
        barJoie -= ((Calendar.getInstance().getTime().getTime()-lastConnection)/hFactor);
        barJoie = (barJoie<0?0:barJoie);
        return (barJoie >= 0);
    }

    public void pet(){
        barJoie += 0.002;
        barJoie = (barJoie>1?1:barJoie);
    }



    public String toString(){
        return "ID: " + this.id.toString()
                + " Name: " + this.name
                + " lastco: " + this.lastConnection
                + " barfaim: " + this.barFaim;
    }

    public void update(){
        faim();
        joie();
        lastConnection = Calendar.getInstance().getTime().getTime();
    }

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
        color = Color.HSVToColor( new float[] {(float)Funfest.randomGenerator(Funfest.stringToSeed(name))*360.0f,1.0f,1.0f}  );
    }

    public long getLastConnection() {
        return lastConnection;
    }
    public void setLastConnection(long lastConnection) {
        this.lastConnection = lastConnection;
    }

    public double getBarFaim() {
        update(); return barFaim;
    }
    public void setBarFaim(double barFaim) {
        this.barFaim = barFaim;
    }

    public double getBarJoie() {
        return barJoie;
    }
    public void setBarJoie(double barJoie) {
        this.barJoie = barJoie;
    }

    public static void toggleHF() {
        hFactor= (hFactor == 6000 ? 3600000 : 6000);
    }
}
