package fr.amdayopa.slime.slimeBoi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.lang.reflect.Field;

import fr.amdayopa.slime.R;
import fr.amdayopa.slime.SlimeApplication;

import static android.content.ContentValues.TAG;

/**
 * Created by Dalil on 20/03/2018.
 */

public class Bar extends SurfaceView implements SurfaceHolder.Callback{
    private double value = 1.0;
    private int v;
    private SurfaceHolder holder = null;
    private UpdateThread updater;
    private Paint paint;
    private RectF vie;
    private RectF fond;
    private int color;

    public Bar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        vie=new RectF();
        fond=new RectF();
        holder = getHolder();
        holder.addCallback(this);
        this.setZOrderMediaOverlay(true);
        setV(attrs);
        disableSurfaceViewLogging();
    }

    public Bar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        vie=new RectF();
        fond=new RectF();
        holder = getHolder();
        holder.addCallback(this);
        this.setZOrderMediaOverlay(true);
        setV(attrs);
        disableSurfaceViewLogging();
    }

    public Bar(Context context, AttributeSet attrs) {
        super(context, attrs);
        vie=new RectF();
        fond=new RectF();
        holder = getHolder();
        holder.addCallback(this);
        this.setZOrderMediaOverlay(true);
        setV(attrs);
        disableSurfaceViewLogging();
    }

    public Bar(Context context, double value) {
        super(context);
        vie=new RectF();
        fond=new RectF();
        this.value = value;
        holder = getHolder();
        holder.addCallback(this);
        this.setScaleX(0.9f);
        this.setScaleY(0.01f);
        this.setZOrderMediaOverlay(true);
        disableSurfaceViewLogging();
    }

    public void setV(AttributeSet attrs){
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.Bar, 0, 0);
        try {
            if(ta.getString(R.styleable.Bar_bar_type).equals("faim")) {
                v=1;
                color = Color.BLUE;
            }
            else if(ta.getString(R.styleable.Bar_bar_type).equals("joie")) {
                v=2;
                color = Color.rgb(255, 0, 120);
            }
        } finally {
            ta.recycle();
        }
    }

    public void setValue(double val){
        value = val;
        value = (value>1?1:value);
        value = (value<0?0:value);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint=new Paint();
        canvas.drawColor(0X0099cc);
        paint.setColor(Color.GRAY);
        fond.set(0,0,canvas.getWidth(),canvas.getHeight());
        canvas.drawRoundRect(fond, 100, 100, paint);
        paint.setColor(color);
        vie.set(0,0,(int)(canvas.getWidth()*value),canvas.getHeight());
        canvas.drawRoundRect(vie, 100, 100, paint);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);
        canvas.drawRoundRect(fond, 100, 100, paint);

    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        setWillNotDraw(false);
        SlimeApplication.getCurrentSlime().setBarFaim(1.0);
        updater = new UpdateThread();
        updater.on = true;
        updater.start();
        Log.d("Plop","bar lancé");
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        updater.on = false;

        boolean joined = false;
        while (!joined) {
            try {
                updater.join();
                joined = true;
            } catch (InterruptedException e) {}

        }
    }

    private void disableSurfaceViewLogging() {
        try {
            Field field = SurfaceView.class.getDeclaredField("DEBUG");
            field.setAccessible(true);
            field.set(null, false);
            Log.i(TAG, "SurfaceView debug disabled");
        } catch (Exception e) {
            Log.e(TAG, "while trying to disable debug in SurfaceView", e);
        }
    }

    private class UpdateThread extends Thread{
        public boolean on = true;

        @SuppressLint("WrongCall")
        @Override
        public void run(){
            while(on){
                Canvas canvas = null;
                switch(v){
                    case 1:
                        setValue(SlimeApplication.getCurrentSlime().getBarFaim());
                    break;
                    case 2:
                        setValue(SlimeApplication.getCurrentSlime().getBarJoie());
                    break;
                }

                try{
                    canvas = holder.lockCanvas();
                    synchronized(holder){
                        if(canvas != null)
                        onDraw(canvas);
                        postInvalidate();
                    }
                }finally{
                    if(canvas != null)
                        holder.unlockCanvasAndPost(canvas);
                }
                try {
                    Thread.sleep(33);
                } catch (InterruptedException e) {}
            }
        }
    }

}

