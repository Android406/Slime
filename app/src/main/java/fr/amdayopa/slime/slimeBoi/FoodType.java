package fr.amdayopa.slime.slimeBoi;

import fr.amdayopa.slime.R;

public enum FoodType{
    RED(0, "otomate"),
    YELLOW(60, "dabricot"),
    GREEN(120, "saladdine"),
    CYAN(180, "artifroid"),
    BLUE(240, "mortille"),
    MAGENTA(300, "robergine");

    private int hue;
    private String src;

    FoodType(int hue, String src){
        this.hue = hue;
        this.src = src;
    }

    public int getHue() {return hue;}
    public String getSrc(){return src;}

    public static FoodType getClosestType(int hue){
        int reste = hue%60;
        hue =  reste<30? hue - (reste) : hue - (reste) + 60;
        if(hue==360) hue = 0;
        return getFoodTypeByHue(hue);
    }

    private static FoodType getFoodTypeByHue(int hue){
        for(FoodType type : FoodType.values()){
            if(type.getHue() == hue) return type;
        }
        return null;
    }
}
