package fr.amdayopa.slime.slimeBoi;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;
import java.util.List;

import fr.amdayopa.slime.MenuActivity;
import fr.amdayopa.slime.StartActivity;
import fr.amdayopa.slime.database.SlimeDAO;
import fr.amdayopa.slime.utils.Pair;

import static fr.amdayopa.slime.utils.Funfest.notif;

public class NotifSpammer extends BroadcastReceiver {

    @Override
    public void onReceive(Context ctx, Intent intent) {
        Intent launchNotif = new Intent(ctx, NotifService.class);
        ctx.startService(launchNotif);

    }
}
