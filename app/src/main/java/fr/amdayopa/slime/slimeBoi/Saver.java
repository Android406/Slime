package fr.amdayopa.slime.slimeBoi;

import android.app.job.JobParameters;
import android.app.job.JobService;

import fr.amdayopa.slime.SlimeApplication;
import fr.amdayopa.slime.database.SlimeDAO;


public class Saver extends JobService {

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        if(SlimeApplication.getCurrentSlime() != null)
            SlimeDAO.saveSlime(getApplication(),SlimeApplication.getCurrentSlime());
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
