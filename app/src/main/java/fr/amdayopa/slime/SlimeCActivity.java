package fr.amdayopa.slime;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import fr.amdayopa.slime.database.SlimeDAO;
import fr.amdayopa.slime.slimeBoi.Slime;
import fr.amdayopa.slime.utils.Funfest;



public class SlimeCActivity extends AppCompatActivity {
    int color = 0xFFFFFF;
    String name = "Slimmy";
    ImageView slm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slime_creator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Animation c = AnimationUtils.loadAnimation(this, R.anim.animation_scroll);
        c.reset();

        Animation a = AnimationUtils.loadAnimation(this, R.anim.animation_breath);
        a.reset();
        slm = findViewById(R.id.slm);
        slm.clearAnimation();
        slm.setAnimation(a);

        EditText et = findViewById(R.id.slimecname);

        et.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                name = String.valueOf(editable);
                slm.setColorFilter(Color.HSVToColor( new float[] {(float)Funfest.randomGenerator(Funfest.stringToSeed(name))*360.0f,1.0f,1.0f}  ));
            }
        });

        Button bt = findViewById(R.id.slimecbutton);
        bt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(SlimeApplication.getCurrentSlime() != null)
                SlimeDAO.saveSlime(getApplication(),SlimeApplication.getCurrentSlime());
                Slime slime = new Slime();
                slime.setName(name);
                SlimeDAO.saveSlime(getApplication(), slime);
                SlimeApplication.setCurrentSlime(slime);
                Intent intent = new Intent(SlimeCActivity.this , StartActivity.class);
                SlimeCActivity.this.startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(SlimeApplication.getCurrentSlime() != null)
            SlimeDAO.saveSlime(getApplication(),SlimeApplication.getCurrentSlime());
    }
}
