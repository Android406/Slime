package fr.amdayopa.slime;

import android.app.AlarmManager;
import android.app.Application;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import fr.amdayopa.slime.database.SlimeDAO;
import fr.amdayopa.slime.slimeBoi.NotifSpammer;
import fr.amdayopa.slime.slimeBoi.Saver;
import fr.amdayopa.slime.slimeBoi.Slime;

public class SlimeApplication extends Application{
    private AlarmManager alert;
    private PendingIntent alarmPint;
    private JobScheduler jobScheduler;

    static private Slime currentSlime;

    @Override
    public void onCreate() {
        super.onCreate();
//        boolean isloaded = SlimeDAO.loadMainSlime(this);
//        if( !isloaded ) {
//            System.out.println("---TOASTZER--- NOT LOADED");
//            makeNewSlime();
//        }
//        if( currentSlime == null) {
//            System.out.println("---TOASTZER--- SLIME IS NULL : ");
//            makeNewSlime();
//        }
//            System.out.println("---TOASTZER--- CurrentSlime : " + currentSlime.toString());

        //

        //alert = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        //Intent intent = new Intent(getApplicationContext(), NotifSpammer.class);

        //alarmPint = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        setAlarmNotif();
        setupJobScheduler();
    }

    @Deprecated
    private void makeNewSlime() {
        Slime s = new Slime();
        s.setName("Nouveau-Slime");
        currentSlime = s;
        SlimeDAO.saveSlime(this, s);
        System.out.println("---TOASTZER--- CreateNewSlime : ");
    }

    public void setupJobScheduler(){
        jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(new JobInfo.Builder(1,
                new ComponentName(this, Saver.class))
                .setPeriodic(60000)
                .build());
    }

    public void setAlarmNotif(){
        alert = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), NotifSpammer.class);
        alarmPint = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alert.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 60000L, 60000L, alarmPint);
    }

    public AlarmManager getAlert() {
        return alert;
    }
    public void setAlert(AlarmManager alert) {
        this.alert = alert;
    }

    public PendingIntent getAlarmPint() {
        return alarmPint;
    }
    public void setAlarmPint(PendingIntent alarmPint) {
        this.alarmPint = alarmPint;
    }

    public static Slime getCurrentSlime() {
        return currentSlime;
    }
    public static void setCurrentSlime(Slime slime) {
        currentSlime = slime;
    }

    public JobScheduler getJobScheduler() {
        return jobScheduler;
    }
    public void setJobScheduler(JobScheduler jobScheduler) {
        this.jobScheduler = jobScheduler;
    }
}
