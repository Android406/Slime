package fr.amdayopa.slime;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import fr.amdayopa.slime.database.SlimeDAO;

public class MenuActivity extends AppCompatActivity {
    MediaPlayer mPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        mPlayer = MediaPlayer.create(MenuActivity.this, R.raw.introsound);
        mPlayer.setLooping(true);
        mPlayer.setVolume(0.5f,0.5f);
        mPlayer.start();
        Animation c = AnimationUtils.loadAnimation(this, R.anim.animation_scroll);
        c.reset();
        Animation a = AnimationUtils.loadAnimation(this, R.anim.animation_float);
        a.reset();
        TextView tv = findViewById(R.id.tvstart);
        tv.clearAnimation();
        tv.setAnimation(a);
        tv.setTypeface(null, Typeface.BOLD);
        Animation b = AnimationUtils.loadAnimation(this, R.anim.animation_splash);
        b.reset();
        ImageView title = findViewById(R.id.title);
        title.clearAnimation();
        title.setAnimation(b);
        View view= (View) findViewById(R.id.startscreen);
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //mPlayer.stop();

                if(SlimeDAO.loadMainSlime(getApplicationContext()) && SlimeApplication.getCurrentSlime()!=null){
                    Intent intent = new Intent(MenuActivity.this, StartActivity.class);
                    MenuActivity.this.startActivity(intent);
                }else{
                    Intent intent = new Intent(MenuActivity.this, SlimeCActivity.class);
                    MenuActivity.this.startActivity(intent);
                }

                finish();
            }
        });
    }
}
