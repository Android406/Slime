package fr.amdayopa.slime.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import java.util.Random;

import fr.amdayopa.slime.R;


public class Funfest{
    public static void notif(String titre, String texte, Class<?> cls, Context ctx, int id) {

        NotificationManager notificationManager =
                (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default",
                    "dTest",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("un chanel de test");
            notificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(ctx, "default");

        Intent intent = new Intent(ctx, cls);
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0, intent, 0);

        notification.setContentIntent(pendingIntent);
        notification.setSmallIcon(R.drawable.pet_button_active);
        notification.setContentTitle(titre);
        notification.setContentText(texte);
        notification.setAutoCancel(true);

        notificationManager.notify(id, notification.build());
    }

    public static long stringToSeed(String s) {
        if (s == null) {
            return 0;
        }
        long hash = 0;
        for (char c : s.toCharArray()) {
            hash = 31L*hash + c;
        }
        return hash;
    }

    public static double randomGenerator(long seed) {
        Random generator = new Random(seed);
        double num = generator.nextDouble();

        return num;
    }
}